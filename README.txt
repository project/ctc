####################
## Overview
####################

INTRODUCTION
------------
What?

A cache tag is a string.

Cache tags are passed around in sets (order doesn't matter) of strings,
so they are typehinted to string[]. They're sets because a single cache
item can depend on (be invalidated by) many cache tags.

Syntax

By convention, they are of the form thing:identifier — and when there's
no concept of multiple instances of a thing, it is of the form thing.
The only rule is that it cannot contain spaces.

There is no strict syntax.

Examples:

    node:5 — cache tag for Node entity 5 (invalidated whenever it changes)

    user:3 — cache tag for User entity 3 (invalidated whenever it changes)

    node_list — list cache tag for Node entities (invalidated whenever any 
     Node entity is updated, deleted or created, i.e. when a listing of nodes
     may need to change). Applicable to any entity type in following format:
     {entity_type}_list.

    config:system.performance — cache tag for the system.performance

    library_info — cache tag for asset libraries

    my_custom_tag - Your Custom tag

This module allows admin users to clear cache tag.

REQUIREMENTS
------------
  No

INSTALLATION
------------
* Install as usual, see http://drupal.org/node/895232 for further information.

CONFIGURATION
------------
* Configure user permissions in Administration » People » Permissions:


CUSTOMIZATION
------------
* Access menu from here : admin/config/development/clear-tag-cache

MAINTAINERS
------------
Current maintainers:
* Ankush Gautam : https://www.drupal.org/u/ankushgautam76gmailcom
  email : ankushgautam76@gmail.com
