<?php

namespace Drupal\ctc\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements ClearTagCacheForm form.
 */
class ClearTagCacheForm extends FormBase {

  /**
   * Drupal\Core\Messenger\MessengerInterface efinition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a Defaultmessenger object.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The MessengerInterface definition.
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'clear_tag_cache_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['tags_name'] = [
      '#title' => $this->t('Enter Tags'),
      '#type' => 'textarea',
      '#required' => TRUE,
      '#attributes' => ['placeholder' => 'Please enter tags seprated by comma'],
      '#default_value' => '',
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    /* Code for validation */
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get Form Values.
    $form_data = $form_state->getvalues();
    $tags_arr = explode(',', $form_data['tags_name']);
    Cache::invalidateTags($tags_arr);
    // Message for user.
    $this->messenger->addStatus('Tags cache clear successfully !');
  }

}
